//
//  ViewController.h
//  TestTwitter
//
//  Created by Peter Pan on 1/10/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ACAccountStore;
@class ACAccount;

@interface ViewController : UIViewController

@property (strong, nonatomic) ACAccountStore *accountStore;
@property (strong, nonatomic) ACAccount *twitterAccount;
@property (strong, nonatomic) ACAccount *facebookAccount;
@property (strong, nonatomic) ACAccount *weiboAccount;

@end
