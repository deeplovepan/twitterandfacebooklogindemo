//
//  ViewController.m
//  TestTwitter
//
//  Created by Peter Pan on 1/10/14.
//  Copyright (c) 2014 Peter Pan. All rights reserved.
//

// for Facebook App , do following steps first
// 1. create facebook app on Facebook dev website
// 2. change facebook app from develop mode to live mode
// 3. add iOS platform for Facebook app, set Bundle ID


#import "ViewController.h"
@import Social;
@import Accounts;

#define AccountTwitterSelectedAccountIdentifier @"TwitterAccountSelectedAccountIdentifier"
#define AccountTwitterAccountAccessGranted  @"TwitterAccountAccessGranted"
#define AccountFacebookAccountAccessGranted       @"FacebookAccountAccessGranted"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UILabel *twitterLabel;

@property (weak, nonatomic) IBOutlet UILabel *facebookLabel;

@end

@implementation ViewController

- (IBAction)twitterLoginButPressed:(id)sender {
    
    [self getTwitterAccount];
    
}

- (IBAction)fbGetFeedButPressed:(id)sender {
    
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                            requestMethod:SLRequestMethodGET
                                                      URL:[NSURL URLWithString:@"https://graph.facebook.com/me/feed"]
                                               parameters:nil];
    
    // 2
    request.account = self.facebookAccount;
    
    // 3
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error)
        {
            // 4
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"There was an error reading your Facebook" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            });
        }
        else
        {
            // 5
            NSError *jsonError;
            NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&jsonError];
            
            if (jsonError)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"There was an error reading your Facebook" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alertView show];
                });
            }
            else
            {
                // 7
                
                NSLog(@"responseJSON %@", responseJSON);
                
                
            }
        }
    }];

}


- (IBAction)facebookLoginButPressed:(id)sender {
    
    [self getFacebookAccount];
}


- (IBAction)getFacebookButPressed:(id)sender {
    
    if(self.facebookAccount)
    {
        [self getFacebookInfo];

    }
}



- (IBAction)getTwitterInfoButPressed:(id)sender {
    
    if(self.twitterAccount)
    {
        [self getTwitterAccountInfo];

    }

}





- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

    self.accountStore = [[ACAccountStore alloc] init];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *twitterId = [defaults objectForKey:@"twitterId"];
    self.twitterAccount = [self.accountStore accountWithIdentifier:twitterId];
  
    if(self.twitterAccount)
    {
        self.twitterLabel.text = [NSString stringWithFormat:@"twitter:%@", self.twitterAccount.username];
    }
    
    NSString *fbId = [defaults objectForKey:@"fbId"];
    self.facebookAccount = [self.accountStore accountWithIdentifier:fbId];
    
    if(self.facebookAccount)
    {
        self.facebookLabel.text = [NSString stringWithFormat:@"facebook:%@", self.facebookAccount.username];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - weibo


- (IBAction)weiboPostImageAndMsg:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"http://api.t.sina.com.cn/statuses/upload.json"];
    NSDictionary *params = @{ @"status" : @"hello"  };

    SLRequest *req  = [SLRequest requestForServiceType:SLServiceTypeSinaWeibo
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:url
                                                       parameters:params];
    NSString *path = [[NSBundle mainBundle] pathForResource:@"peterpan.jpg" ofType:nil];
    NSData *imageData = [[NSData alloc] initWithContentsOfFile:path];
    
    [req addMultipartData:imageData
                     withName:@"pic"
                         type:@"image/jpeg"
                     filename:@"image.jpg"];
    
    req.account = self.weiboAccount;
    
    [req performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
     {
         // Log the result
         if(error)
         {
             NSLog(@"error");
         }
         else
         {
             NSError *jsonError;
             NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                          options:NSJSONReadingAllowFragments
                                                                            error:&jsonError];
             NSLog(@"responseJSON %@", responseJSON);
             
         }
     }];


}

- (IBAction)weiboLoginButPressed:(id)sender {

    ACAccountStore *account = [[ACAccountStore alloc] init];
    ACAccountType *accountType = [account accountTypeWithAccountTypeIdentifier:
                                  ACAccountTypeIdentifierSinaWeibo];
    
    __weak ViewController *weakSelf = self;
    [account requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
       
        if(granted)
        {
            NSArray *arrayOfAccounts = [account
                                        accountsWithAccountType:accountType];
            if(arrayOfAccounts.count > 0)
            {
                weakSelf.weiboAccount = [arrayOfAccounts firstObject];
                NSLog(@"account %@", weakSelf.weiboAccount);
                
                
            }
        }
    }];
}



#pragma mark - fb

-(IBAction)postPhotoAndMessage
{
    NSDictionary *parameters = @{@"message": @"https://itunes.apple.com/app/id341232718"};
    
    SLRequest *facebookRequest = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                    requestMethod:SLRequestMethodPOST
                                                              URL:[NSURL    URLWithString:@"https://graph.facebook.com/me/photos"]
                                                       parameters:parameters];
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"peterpan.jpg" ofType:nil];
    NSData *imageData = [[NSData alloc] initWithContentsOfFile:path];
    
    [facebookRequest addMultipartData: imageData
                             withName:@"source"
                                 type:@"multipart/form-data"
                             filename:@"TestImage"];
    
    facebookRequest.account = self.facebookAccount;
    
    [facebookRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error)
    {
        // Log the result
        if(error)
        {
            NSLog(@"error");
        }
        else
        {
            NSError *jsonError;
            NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&jsonError];
            NSLog(@"responseJSON %@", responseJSON);
            
        }
    }];
}

-(void)getFacebookInfo
{
    NSDictionary *paraDic = @{ @"fields" : @"picture,email,name" };
    // 1
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                            requestMethod:SLRequestMethodGET
                                                      URL:[NSURL URLWithString:@"https://graph.facebook.com/me"]
                                               parameters:paraDic];
    
    // 2
    request.account = self.facebookAccount;
    
    // 3
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        if (error)
        {
            // 4
            dispatch_async(dispatch_get_main_queue(), ^{
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"There was an error reading your Facebook" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                [alertView show];
            });
        }
        else
        {
            // 5
            NSError *jsonError;
            NSDictionary *responseJSON = [NSJSONSerialization JSONObjectWithData:responseData
                                                                         options:NSJSONReadingAllowFragments
                                                                           error:&jsonError];
            
            if (jsonError)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"There was an error reading your Facebook" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
                    [alertView show];
                });
            }
            else
            {
                // 7
                
                NSDictionary *dic = responseJSON;
                NSMutableString *photoUrl = [dic[@"picture"][@"data"][@"url"] mutableCopy];
                NSRange range = NSMakeRange(0, photoUrl.length);
                [photoUrl replaceOccurrencesOfString:@"q.jpg" withString:@"n.jpg" options:NSCaseInsensitiveSearch range:range];
                NSLog(@"name %@ email %@ image %@", dic[@"name"], dic[@"email"], photoUrl
                      );
                
            }
        }
    }];
    
    
}


- (void)getFacebookAccount
{
    // 1
    ACAccountType *facebookAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    
    __weak ViewController *weakSelf = self;
    
    // 2
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        // 3
        
        
        NSDictionary *facebookOptions = @{ ACFacebookAppIdKey : @"507702772613239",
                                           ACFacebookPermissionsKey : @[@"email"],
                                           ACFacebookAudienceKey : ACFacebookAudienceFriends };
        
        
        // 4
        [self.accountStore requestAccessToAccountsWithType:facebookAccountType
                                                   options:facebookOptions
                                                completion:^(BOOL granted, NSError *error) {
                                                    // 5
                                                    
                                                    
                                                    if (granted)
                                                    {
                                                        
                                                        NSDictionary *facebookOptions = @{ ACFacebookAppIdKey : @"507702772613239",
                                                                                           ACFacebookPermissionsKey : @[@"publish_stream"],
                                                                                           ACFacebookAudienceKey : ACFacebookAudienceFriends };
                                                        
                                                        
                                                        
                                                        
                                                        [self.accountStore requestAccessToAccountsWithType:facebookAccountType
                                                                                                   options:facebookOptions
                                                                                                completion:^(BOOL granted, NSError *error) {
                                                                                                    
                                                                                                    weakSelf.facebookAccount = [[weakSelf.accountStore accountsWithAccountType:facebookAccountType] lastObject];
                                                                                                    
                                                                                                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                                                                    [defaults setObject:weakSelf.facebookAccount.identifier forKey:@"fbId"];
                                                                                                    [defaults synchronize];
                                                                                                    
                                                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                                                        
                                                                                                        NSLog(@"login ok");
                                                                         
                                                                                                        self.facebookLabel.text = [NSString stringWithFormat:@"facebook:%@", weakSelf.facebookAccount.username];
                                                                                                        
                                                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:AccountFacebookAccountAccessGranted
                                                                                                                                                            object:nil];
                                                                                                    });
                                                                                                    
                                                                                                    
                                                                                                }];
                                                        
                                                    }
                                                    // 6
                                                    else
                                                    {
                                                        // 7
                                                        if (error)
                                                        {
                                                            NSLog(@"error %@", error);
                                                            
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                                                                                    message:@"There was an error retrieving your Facebook account, make sure you have an account setup in Settings and that access is granted"
                                                                                                                   delegate:nil
                                                                                                          cancelButtonTitle:@"Dismiss"
                                                                                                          otherButtonTitles:nil];
                                                                [alertView show];
                                                            });
                                                        }
                                                        // 8
                                                        else
                                                        {
                                                            dispatch_async(dispatch_get_main_queue(), ^{
                                                                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Facebook"
                                                                                                                    message:@"Access to Facebook was not granted. Please go to the device settings and allow access for iSocial"
                                                                                                                   delegate:nil
                                                                                                          cancelButtonTitle:@"Dismiss"
                                                                                                          otherButtonTitles:nil];
                                                                [alertView show];
                                                            });
                                                        }
                                                    }
                                                }];
    });
}


#pragma mark - twitter


- (IBAction)twitterPostPhotoAndMessage:(id)sender {
    
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com"
                  @"/1.1/statuses/update_with_media.json"];
    NSDictionary *params = @{@"status" : @"hello"};
    SLRequest *request = [SLRequest requestForServiceType:SLServiceTypeTwitter
                                            requestMethod:SLRequestMethodPOST
                                                      URL:url
                                               parameters:params];

    NSString *path = [[NSBundle mainBundle] pathForResource:@"peterpan.jpg" ofType:nil];
    NSData *imageData = [[NSData alloc] initWithContentsOfFile:path];
    
    [request addMultipartData:imageData
                     withName:@"media[]"
                         type:@"image/jpeg"
                     filename:@"image.jpg"];
    [request setAccount:self.twitterAccount];
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
       
        if (responseData) {
            
             NSInteger statusCode = urlResponse.statusCode;
             if (statusCode >= 200 && statusCode < 300) {
                 NSLog(@"ok");
                 
             }

        }
        else
        {
            
        }
        
    }];
}


- (void )getTwitterAccount
{
    
    ACAccountType *twitterAccountType = [self.accountStore accountTypeWithAccountTypeIdentifier: ACAccountTypeIdentifierTwitter];
    
    __weak ViewController *weakSelf = self;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [weakSelf.accountStore requestAccessToAccountsWithType:twitterAccountType
                                                       options:nil
                                                    completion:^(BOOL granted, NSError *error) {
                                                        
                                                        
                                                        // 1
                                                        NSArray *twitterAccounts = [weakSelf.accountStore accountsWithAccountType:twitterAccountType];
                                                        
                                                        
                                                        
                                                        if (granted && twitterAccounts)
                                                        {
                                                            
                                                            
                                                            // 2
                                                            NSString *twitterAccountIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:AccountTwitterSelectedAccountIdentifier];
                                                            weakSelf.twitterAccount = [weakSelf.accountStore accountWithIdentifier:twitterAccountIdentifier];
                                                            
                                                           
                                                            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                            
                                                            [defaults setObject:weakSelf.twitterAccount.identifier forKey:@"twitterId"];
                                                            
                                                            [defaults synchronize];
                                                            
                                                            // 3
                                                            if (weakSelf.twitterAccount)
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    
                                                                    weakSelf.twitterLabel.text = [NSString stringWithFormat:@"twitter:%@",weakSelf.twitterAccount.username];
                                                                    
                                                                    
                                                                    
                                                                    
                                                                    [[NSNotificationCenter defaultCenter] postNotificationName:AccountTwitterAccountAccessGranted object:nil];
                                                                });
                                                            }
                                                            else
                                                            {
                                                                // 4
                                                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:AccountTwitterSelectedAccountIdentifier];
                                                                [[NSUserDefaults standardUserDefaults] synchronize];
                                                                
                                                                // 5
                                                                if(twitterAccounts.count == 0)
                                                                {
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Please set twitter account in Setting" message:nil delegate:nil cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                                                                        [alertView show];
                                                                    });
                                                                    
                                                                }
                                                                else if (twitterAccounts.count > 1)
                                                                {
                                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Twitter"
                                                                                                                        message:@"Select one of your Twitter Accounts"
                                                                                                                       delegate:weakSelf
                                                                                                              cancelButtonTitle:@"Cancel"
                                                                                                              otherButtonTitles:nil];
                                                                    
                                                                    for (ACAccount *account in twitterAccounts)
                                                                    {
                                                                        [alertView addButtonWithTitle:account.accountDescription];
                                                                    }
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                        [alertView show];
                                                                    });
                                                                }
                                                                // 6
                                                                else
                                                                {
                                                                    weakSelf.twitterAccount = [twitterAccounts lastObject];
                                                                    
                                                                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                                                                    
                                                                    [defaults setObject:weakSelf.twitterAccount.identifier forKey:@"twitterId"];
                                                                    
                                                                    [defaults synchronize];
                                                                    
                                                                    dispatch_async(dispatch_get_main_queue(), ^{
                                                                     
                                                                        weakSelf.twitterLabel.text =
                                                                        [NSString stringWithFormat:@"twitter:%@",weakSelf.twitterAccount.username];
                                                                        

                                                                        
                                                                        [[NSNotificationCenter defaultCenter] postNotificationName:AccountTwitterAccountAccessGranted
                                                                                                                            object:nil];
                                                                    });
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (error)
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Twitter"
                                                                                                                        message:@"There was an error retrieving your Twitter account, make sure you have an account setup in Settings and that access is granted for iSocial"
                                                                                                                       delegate:nil
                                                                                                              cancelButtonTitle:@"Dismiss"
                                                                                                              otherButtonTitles:nil];
                                                                    [alertView show];
                                                                });
                                                            }
                                                            else
                                                            {
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Twitter"
                                                                                                                        message:@"Access to Twitter was not granted. Please go to the device settings and allow access for iSocial"
                                                                                                                       delegate:nil
                                                                                                              cancelButtonTitle:@"Dimiss"
                                                                                                              otherButtonTitles:nil];
                                                                    [alertView show];
                                                                });
                                                            }
                                                        }
                                                    }];
    });
}



-(void)getTwitterAccountInfo
{
    NSURL *url = [NSURL URLWithString:@"https://api.twitter.com/1.1/users/show.json"];
    
    
    
    NSDictionary *params = @{@"user_id" : [[self.twitterAccount valueForKey:@"properties"] valueForKey:@"user_id"]};
    
    SLRequest *request =
    [SLRequest requestForServiceType:SLServiceTypeTwitter
                       requestMethod:SLRequestMethodGET
                                 URL:url
                          parameters:params];
    [request setAccount:self.twitterAccount];
    
    [request performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
        
        if(responseData)
        {
            
            if (urlResponse.statusCode >= 200 && urlResponse.statusCode < 300) {
                
                
                NSDictionary* jsonDic = [NSJSONSerialization
                                         JSONObjectWithData:responseData //1
                                         options:NSJSONReadingAllowFragments
                                         error:nil ];
                NSMutableString *imageUrl = [jsonDic[@"profile_image_url"] mutableCopy];
                NSRange range = NSMakeRange(0, imageUrl.length);
                [imageUrl replaceOccurrencesOfString:@"_normal.jpeg" withString:@"" options:NSCaseInsensitiveSearch range:range];
                
                NSLog(@"name %@ profile %@", jsonDic[@"name"], imageUrl);
                
            }
        }
    }];
    
    
}

@end
